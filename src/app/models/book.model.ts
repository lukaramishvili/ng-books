
export class Book {
  id: number;
  name: string;
  author: string;
  isbn: string;
  constructor({ id, name, author, isbn }) {
    this.id = id;
    this.name = name;
    this.author = author;
    this.isbn = isbn;
  }
  static Empty = () => new Book({ id: 0, name: '', author: '', isbn: '' });
  toString = () => `Book ${this.name} by ${this.author}; issue N. ${this.isbn}.`;
}
