import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BooksDashboardComponent } from './books-dashboard/books-dashboard.component';

const routes: Routes = [
  { path: '', redirectTo: '/books/dashboard', pathMatch: 'full' },
  { path: 'books/dashboard', component: BooksDashboardComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
