import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ToastaModule } from 'ngx-toasta';

import { BooksDashboardComponent } from './books-dashboard/books-dashboard.component';
import { BooksListComponent } from './books-list/books-list.component';
import { EditBookFormComponent } from './edit-book-form/edit-book-form.component';

@NgModule({
  declarations: [
    AppComponent,
    BooksDashboardComponent,
    BooksListComponent,
    EditBookFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ToastaModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
