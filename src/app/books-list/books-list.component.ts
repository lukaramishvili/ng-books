import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Book } from '../models/book.model';

@Component({
  selector: 'app-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.scss']
})
export class BooksListComponent implements OnInit {

  @Input() books: Book[];

  @Output() onBookEdit = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  editBook(book: Book) {
    this.onBookEdit.emit(book.id);
  }

}
