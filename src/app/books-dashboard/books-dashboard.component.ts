import { Component, OnInit } from '@angular/core';

import { EditBookFormComponent } from '../edit-book-form/edit-book-form.component';

import { BooksListComponent } from '../books-list/books-list.component';
import { Book } from '../models/book.model';

import { ToastaService, ToastaConfig, ToastOptions, ToastData } from 'ngx-toasta';

@Component({
  selector: 'app-books-dashboard',
  templateUrl: './books-dashboard.component.html',
  styleUrls: ['./books-dashboard.component.scss']
})
export class BooksDashboardComponent implements OnInit {

  books: Book[]; // = require('../../../data/books.json');
  /* [
     new Book({ id: 1, name: "იადიგარ დაუდი", author: "ხალხური", isbn: "978-9941-989-14-8" }),
     new Book({ id: 2, name: "ვეფხისტყაოსანი", author: "შოთა რუსთაველი", isbn: "978-9941-478-18-5" }),
     new Book({ id: 3, name: "ქართლის ცხოვრება", author: "ლეონტი მროველი და სხვა მრავალი", isbn: "978-9941-445-52-1" }),
     ]
  */

  editingBook: Book = Book.Empty();

  constructor(private toastaService: ToastaService, private toastaConfig: ToastaConfig) {
  }

  ngOnInit() {
    let strSavedBooks = localStorage.getItem("books");
    if (!strSavedBooks) {
      this.loadDummyBooks();
      return;
    }
    try { this.books = JSON.parse(strSavedBooks).map(objBook => new Book(objBook)); }
    catch {
      this.loadDummyBooks();
    }
  }

  loadDummyBooks() {
    // default data
    this.books = [
      new Book({ id: 1, name: "იადიგარ დაუდი", author: "ხალხური", isbn: "978-9941-989-14-8" }),
      new Book({ id: 2, name: "ვეფხისტყაოსანი", author: "შოთა რუსთაველი", isbn: "978-9941-478-18-5" }),
      new Book({ id: 3, name: "ქართლის ცხოვრება", author: "ლეონტი მროველი და სხვა მრავალი", isbn: "978-9941-445-52-1" }),
    ];
  }

  nextBookId() {
    let maxId = this.books.reduce((prevMax, bk) => Math.max(prevMax, bk.id), 0);
    if (!(maxId > 0)) {
      maxId = 0;
    }
    return maxId + 1;
  }

  bookSaved(book: Book) {
    if (!(book.id > 0)) {
      // this is a new book; increment ID
      book.id = this.nextBookId();
    }
    // existing book, so replace
    if (this.books.map(b => b.id).indexOf(book.id) > -1) {
      this.books = this.books.map(b => b.id === book.id ? book : b);
    } else {
      this.books.push(book);
    }
    //console.log(`saved ${book}`);
    this.editingBook = Book.Empty();
    //
    this.saveToStorage();
    //
    this.showSuccessMessage();
  }

  saveToStorage() {
    localStorage.setItem("books", JSON.stringify(this.books));
  }

  showSuccessMessage() {
    var toastOptions: ToastOptions = {
      title: "შეტყობინება",
      msg: "წიგნი წარმატებით დაემატა",
      showClose: true,
      timeout: 5000,
      theme: 'default'
    };
    // Add see all possible types in one shot
    this.toastaService.success(toastOptions);
  }

  bookEditClicked(bookId: number) {
    this.editingBook = this.books.filter(b => b.id === bookId)[0];
    console.log(`${this.editingBook}`);
    //console.log(`edit book #${bookId}`);
  }

  bookSaveCanceled() {
    this.editingBook = Book.Empty();
  }

}
