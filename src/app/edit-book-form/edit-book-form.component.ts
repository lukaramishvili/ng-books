import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Book } from '../models/book.model';

import { ReactiveFormsModule, FormsModule, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-book-form',
  templateUrl: './edit-book-form.component.html',
  styleUrls: ['./edit-book-form.component.scss']
})
export class EditBookFormComponent implements OnInit {

  private _book: Book;

  @Input('book')
  set book(value: Book) {
    this._book = value;
    this.bookForm.controls['name'].setValue(this._book.name);
    this.bookForm.controls['author'].setValue(this._book.author);
    this.bookForm.controls['isbn'].setValue(this._book.isbn);
  }

  @Output() onSave = new EventEmitter<Book>();
  @Output() onCancel = new EventEmitter<Book>();

  defaultBook: Book = Book.Empty();

  bookForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.bookForm = fb.group({
      'name': [
        this.defaultBook.name,
        Validators.compose([
          Validators.required
        ])
      ],
      'author': [
        this.defaultBook.author,
        Validators.compose([
          Validators.required
        ])
      ],
      'isbn': [
        this.defaultBook.isbn,
        Validators.compose([
          Validators.required,
          this.isbnValidator
        ])
      ]
    });
  }

  ngOnInit() {
    //this.bookForm.valueChanges.subscribe(values => console.log(values));
  }

  isbnValidator(control: FormControl): { [k: string]: boolean } {
    if (!/^[0-9-]{13,17}$/gi.test(control.value)) {
      return { incorrectIsbn: true };
    } else return null;
  }

  onBookFormSubmit(form: any): boolean {
    if (this.bookForm.status !== "VALID") {
      return false;
    }
    //form
    let resultBook = new Book({
      id: this._book.id || 0,
      name: this.bookForm.controls["name"].value,
      author: this.bookForm.controls["author"].value,
      isbn: this.bookForm.controls["isbn"].value,
    });
    this.onSave.emit(resultBook);
    this.bookForm.reset();
    return false;
  }

  cancelEditing() {
    this.onCancel.emit();
  }

}
